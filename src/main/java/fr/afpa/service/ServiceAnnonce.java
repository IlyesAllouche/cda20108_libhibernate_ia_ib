package fr.afpa.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.beans.Annonce;
import fr.afpa.control.GeneralControl;
import fr.afpa.control.SaisieControl;
import fr.afpa.dao.DaoAnnonce;
import fr.afpa.vue.Menu;

public class ServiceAnnonce {

	/**
	 * 
	 * @return
	 */
	public static Annonce posterAnnonce() {
		
		Scanner in = new Scanner(System.in);
		
		
		System.out.println("Entrez le Titre : ");
		String titre = in.nextLine();
		
		System.out.println("Entrez la Quantit� : ");
		int quantit� = in.nextInt();
		in.nextLine();
		
		System.out.println("Entrez le Prix : ");
		float prix = in.nextFloat();
		in.nextLine();
		
		while (SaisieControl.prix(prix) == false) {
			System.out.println("Re-Entrez le Prix : ");
			prix = in.nextFloat();
			in.nextLine();
		}
		
		
		System.out.println("Entrez la Remise : ");
		float remise = in.nextFloat();
		in.nextLine();
		
		while (SaisieControl.remise(remise) == false) {
			System.out.println("Re-Entrez la Remise : ");
			remise = in.nextFloat();
			in.nextLine();
		}
		
		
		System.out.println("Entrez la Maison d'�dition : ");
		String maisonEdition = in.nextLine();
		
		
		System.out.println("Entrez l'ISBN : ");
		String isbn = in.nextLine();
		
		
		System.out.println("Entrez la date d'�dition (AAAA-MM-DD) : ");
		String dateEdition = in.nextLine();
		
		while (SaisieControl.date(dateEdition) == false) {
			System.out.println("Re-Entrez la date d'�dition (AAAA-MM-DD) : ");
			dateEdition = in.nextLine();
		}
		
		
		System.out.println("Entrez la date d'annnonce (AAAA-MM-DD) : ");
		String dateAnnonce = in.nextLine();
		
		while (SaisieControl.date(dateAnnonce) == false) {
			System.out.println("Re-Entrez la date d'annnonce (AAAA-MM-DD) : ");
			dateAnnonce = in.nextLine();
		}
		
		
		
		Annonce annonce = new Annonce();
		annonce.setDateAnnonce(LocalDate.parse(dateAnnonce));
		annonce.setDateEdition(LocalDate.parse(dateEdition));
		annonce.setMaisonEdition(maisonEdition);
		annonce.setIsbn(isbn);
		annonce.setPrix(prix);
		annonce.setQuantite(quantit�);
		annonce.setRemise(remise);
		annonce.setTitre(titre);
		annonce.setPrixTotal(prix * quantit�);

		
		return annonce;
		
		
	}

	/**
	 * 
	 * @param listAnnonce
	 */
	public static void listerAnnonces(ArrayList<Annonce> listAnnonce) {
		System.out.println("Voici vos Annonces : ");
		for (Annonce annonce : listAnnonce) {
			System.out.println(annonce);
		}
	}

	/**
	 * 
	 * @return
	 */
	public static Annonce modifierAnnonce() {
		GeneralControl.listerSelfAnnonces();
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Entrez l'ID de l'annonce � modifier : ");
		int id = in.nextInt();
		in.nextLine();
		
		Annonce annonceTemp = new Annonce();
		annonceTemp.setId(id);

		System.out.println("Annonce choisie : ");
		Annonce annonceChoisie = DaoAnnonce.recupModifAnnonce(annonceTemp);
		System.out.println(annonceChoisie);
		
		boolean condition = true;
		String choix = "";
		
		do {
			
			Menu.menuModifAnnonce();
			
			System.out.println("Que voulez-vous ? : ");
			choix = in.nextLine();
			
			if (choix.equals("1")) {
				
				System.out.println("Entrez le nouveau titre : ");
				String titre = in.nextLine();
				annonceChoisie.setTitre(titre);
				
				return annonceChoisie;
				
			}else if (choix.equals("2")) {
				
				System.out.println("Entrez la nouvelle quantit� : ");
				int quantite = in.nextInt();
				in.nextLine();
				annonceChoisie.setQuantite(quantite);
				annonceChoisie.setPrixTotal(quantite * annonceChoisie.getPrix());

				return annonceChoisie;
				
			}else if (choix.equals("3")) {
				
				System.out.println("Entrez le nouveau prix : ");
				float prix = in.nextFloat();
				in.nextLine();
				annonceChoisie.setPrix(prix);
				annonceChoisie.setPrixTotal(prix * annonceChoisie.getQuantite());
				
				return annonceChoisie;
				
			}else if (choix.equals("4")) {
				
				System.out.println("Entrez la nouvelle remise : ");
				float remise = in.nextFloat();
				in.nextLine();

				while (SaisieControl.remise(remise) == false) {
					System.out.println("Re-Entrez la Remise : ");
					remise = in.nextFloat();
					in.nextLine();
				}
				
				annonceChoisie.setRemise(remise);

				return annonceChoisie;
				
			}else if (choix.equals("5")) {
				
				System.out.println("Entrez la nouvelle maison d'�dition : ");
				String maison = in.nextLine();
				annonceChoisie.setMaisonEdition(maison);

				return annonceChoisie;
				
			}else if (choix.equals("6")) {
				
				System.out.println("Entrez un nouvel ISBN : ");
				String isbn = in.nextLine();
				annonceChoisie.setIsbn(isbn);

				return annonceChoisie;
				
			}else if (choix.equals("7")) {
				
				System.out.println("Entrez une nouvelle date d'�dition : ");
				String dateEdition = in.nextLine();

				while (SaisieControl.date(dateEdition) == false) {
					System.out.println("Re-Entrez la date d'�dition (AAAA-MM-DD) : ");
					dateEdition = in.nextLine();
				}
				annonceChoisie.setDateEdition(LocalDate.parse(dateEdition));

				return annonceChoisie;
				
			}else if (choix.equals("8")) {
				
				System.out.println("Entrez une nouvelle date d'annonce : ");
				String dateAnnonce = in.nextLine();

				while (SaisieControl.date(dateAnnonce) == false) {
					System.out.println("Re-Entrez la date d'�dition (AAAA-MM-DD) : ");
					dateAnnonce = in.nextLine();
				}
				annonceChoisie.setDateAnnonce(LocalDate.parse(dateAnnonce));

				return annonceChoisie;
				
			}else if (choix.equals("9")) {
				condition = false;
			}else {
				System.out.println("Entrez une saisie correct");
			}
			
		} while (condition);
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public static Annonce supprimerAnnonce() {
		GeneralControl.listerSelfAnnonces();
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Entrez l'ID de l'annonce � supprimer : ");
		int id = in.nextInt();
		in.nextLine();
		
		Annonce annonceTemp = new Annonce();
		annonceTemp.setId(id);

		System.out.println("Annonce choisie : ");
		Annonce annonceChoisie = DaoAnnonce.recupModifAnnonce(annonceTemp);
		System.out.println(annonceChoisie);
		
		return annonceTemp;
		
	}

	/**
	 * 
	 */
	public static void listerFiltre() {
		
		Scanner in = new Scanner(System.in);
		
		boolean condition = true;
		String choix = "";
		
		do {
			
			Menu.menuFiltre();
			
			System.out.println("Que voulez-vous ? : ");
			choix = in.nextLine();
			
			if (choix.equals("1")) {
				
				System.out.println("Entrez la ville : ");
				String ville = in.nextLine();
				listerAnnonces(DaoAnnonce.listerAnnonceVille(ville));
				
			}else if (choix.equals("2")) {
				
				System.out.println("Entrez l'ISBN : ");
				String isbn = in.nextLine();
				listerAnnonces(DaoAnnonce.listerAnnonceISBN(isbn));
				
			}else if (choix.equals("3")) {
				
				System.out.println("Entrez le titre : ");
				String titre = in.nextLine();
				
				System.out.println("Voici les annonces correspondante : ");
				listerAnnonces(DaoAnnonce.recupAnnonceByTitre(titre));
				
				
			}else if (choix.equals("4")) {
				condition = false;
			}else {
				System.out.println("Entrez une saisie correct");
			}
			
		} while (condition);
		
	}
	
}
