package fr.afpa.beans;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@NamedQuery(name= "annonceFindById", query= "from Utilisateur p where p.id = :param")

@Entity
public class Annonce {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String titre;
	private int quantite;
	private float prix;
	private float prixTotal;
	private float remise;
	private String maisonEdition;
	private String isbn;
	private LocalDate dateEdition;
	private LocalDate dateAnnonce;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id")
	private Utilisateur utilisateur;
}
