package fr.afpa.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Entity
@NamedQuery(name= "userFindById", query= "from Utilisateur p where p.id = :param")
public class Utilisateur {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	private String nom;
	private String prenom;
	private String mail;
	private String telephone;
	private String nomLibrairie;
	private Connexion login;
	

	@OneToMany(mappedBy = "utilisateur")
	private List<Annonce> listAnnonce;
	
	@OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "log")
	private Connexion connexion;
	
	@OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "adresse")
	private Adresse adresse;
	
}
