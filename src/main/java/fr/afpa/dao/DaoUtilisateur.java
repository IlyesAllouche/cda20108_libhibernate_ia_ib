package fr.afpa.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Connexion;
import fr.afpa.beans.Utilisateur;
import fr.afpa.utils.HibernateUtils;


public class DaoUtilisateur {

	private static Session session = null;
	
	/**
	 * 
	 * @param log
	 * @return
	 */
	public static Utilisateur recupUser(Connexion log) {
		return null;
		
	}
	
	/**
	 * 
	 * @param log
	 * @param user
	 * @return
	 */
	public static Utilisateur recupUserByName(Connexion log, Utilisateur user) {
		return user;
		
	}
	
	/**
	 * 
	 * @param user
	 */
	public static void insertInto(Utilisateur user) {
		
		session = HibernateUtils.getSession();
		
		
		/*
		ArrayList<Annon> listV = new ArrayList<Voiture>();
		listV.add(voiture);
		
		
		personne.setListVoiture(listV);
		voiture.setPersonne(personne);
		*/
		
	       
        Transaction tx = session.beginTransaction();
        
        session.save(user);
        
        tx.commit();
        session.close();
	}
	
	
	/**
	 * 
	 * @param user
	 */
	public static void updateCompte(Utilisateur user) {
		
	}
	
	
	/**
	 * 
	 * @param user
	 */
	public static void deleteCompte(Utilisateur user) {
		
	}
	
	
}
