package fr.afpa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.control.GeneralControl;

public class DaoAnnonce {
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	public static ArrayList<Annonce> recupSelfAnnonces(Utilisateur user) {
		
		ResultSet listAnnonce = null;
		ArrayList<Annonce> retourAnnonce = new ArrayList<Annonce>();
		
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			
			String strQuery = "SELECT * FROM annonce where iduser = " + String.valueOf(user.getId());
			
			Statement stServices = conn.createStatement();
			
			listAnnonce = stServices.executeQuery(strQuery);
			
			Annonce annonce = null;
			
			while (listAnnonce.next()) {
					annonce = new Annonce();
					annonce.setId(listAnnonce.getInt(1));
					annonce.setTitre(listAnnonce.getString("titre"));
					annonce.setQuantite(listAnnonce.getInt("quantite"));
					annonce.setPrix(listAnnonce.getFloat("prix"));
					annonce.setPrixTotal(listAnnonce.getFloat("prixtotal"));
					annonce.setMaisonEdition(listAnnonce.getString("edition"));
					annonce.setIsbn(listAnnonce.getString("isbn"));
					annonce.setDateEdition(LocalDate.parse(listAnnonce.getString("dateedition")));
					annonce.setDateAnnonce(LocalDate.parse(listAnnonce.getString("dateannonce")));

					retourAnnonce.add(annonce);
				}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retourAnnonce;
	}
	
	/**
	 * 
	 * @param annonce
	 * @return
	 */
	public static Annonce recupModifAnnonce(Annonce annonce) {
		
		ResultSet listAnnonce = null;
		Annonce annonceRecup = null;
		
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			
			String strQuery = "SELECT * FROM annonce where idannonce = " + String.valueOf(annonce.getId());
			
			Statement stServices = conn.createStatement();
			
			listAnnonce = stServices.executeQuery(strQuery);
			
			
			while (listAnnonce.next()) {
				annonceRecup = new Annonce();
				annonceRecup.setId(listAnnonce.getInt(1));
				annonceRecup.setTitre(listAnnonce.getString("titre"));
				annonceRecup.setQuantite(listAnnonce.getInt("quantite"));
				annonceRecup.setPrix(listAnnonce.getFloat("prix"));
				annonceRecup.setPrixTotal(listAnnonce.getFloat("prixtotal"));
				annonceRecup.setMaisonEdition(listAnnonce.getString("edition"));
				annonceRecup.setIsbn(listAnnonce.getString("isbn"));
				annonceRecup.setDateEdition(LocalDate.parse(listAnnonce.getString("dateedition")));
				annonceRecup.setDateAnnonce(LocalDate.parse(listAnnonce.getString("dateannonce")));

				}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return annonceRecup;
	}
	
	/**
	 * 
	 * @param annonce
	 */
	public static void insertInto(Annonce annonce) {
		
	}
	
	/**
	 * 
	 * @param annonce
	 */
	public static void updateModif(Annonce annonce) {
		
		if (annonce != null) {
		}
	}

	/**
	 * 
	 * @param annonce
	 */
	public static void deleteAnnonce(Annonce annonce) {
		
	}

	/**
	 * 
	 * @param titre
	 * @return
	 */
	public static ArrayList<Annonce> recupAnnonceByTitre(String titre) {
		return null;
		
	}
	
	/**
	 * 
	 * @param isbn
	 * @return
	 */
	public static ArrayList<Annonce> listerAnnonceISBN(String isbn) {
		return null;
		
	}

	/**
	 * 
	 * @param ville
	 * @return
	 */
	public static ArrayList<Annonce> listerAnnonceVille(String ville) {
		return null;
	
	}
	
}
